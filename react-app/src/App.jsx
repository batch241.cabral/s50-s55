import React from "react";
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
const App = () => (
    <>
        <Router>
            <NavBar />
            <Container>
                <Routes>
                    <Route path='*' element={<Home />} />
                    <Route path='/' element={<Home />} />
                    <Route path='/courses' element={<Courses />} />
                    <Route path='/register' element={<Register />} />
                    <Route path='/login' element={<Login />} />
                    <Route path='/logout' element={<Logout />} />
                </Routes>
            </Container>
        </Router>
    </>
);

export default App;
