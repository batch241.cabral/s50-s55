import { Card, Button } from "react-bootstrap";
import coursesData from "../data/coursesData";
import React, { useState, useEffect } from "react";

function CourseList() {
    const [enrolleesCount, setEnrolleesCount] = useState(0);

    // const handleClick = (id) => {
    //     setEnrolleesCount((prevEnrollees) => {
    //         const enrollees = prevEnrollees[id] ? prevEnrollees[id] + 1 : 1;
    //         if (enrollees > 30) {
    //             coursesData.forEach((course) => {
    //                 if (course.id === id) {
    //                     course.onOffer = false;
    //                     alert("No more seats available");
    //                 }
    //             });
    //         }
    //         return { ...prevEnrollees, [id]: enrollees };
    //     });
    // };
    const [isOpen, setIsOpen] = useState(true);
    const [seats, setSeats] = useState(30);

    const handleClick = (id) => {
        const courseSeats = (enrolleesCount[id] || 0) + 1;

        if (courseSeats > 30) {
            alert("No more seats available");
        } else {
            setSeats(seats - 1);
            console.log(seats);
            setEnrolleesCount({ ...enrolleesCount, [id]: courseSeats });
        }
    };

    useEffect(() => {
        if (seats === 0) {
            setIsOpen(false);
            coursesData.forEach((course) => {
                if (course.id in enrolleesCount) {
                    course.onOffer = false;
                }
            });
        }
    });

    return coursesData.map((course) => (
        <Card
            key={course.id}
            style={{
                marginBottom: "10px",
            }}
        >
            <Card.Header as='h5'>{course.name}</Card.Header>
            <Card.Body>
                <Card.Title>Description</Card.Title>
                <Card.Text>{course.description}</Card.Text>
                <Card.Title>Price:</Card.Title>
                <Card.Text> PhP {course.price.toLocaleString()}</Card.Text>
                <Card.Title>Enrollees: </Card.Title>
                <Card.Text> {enrolleesCount[course.id] || 0} Enrollees</Card.Text>

                <Button
                    disabled={!course.onOffer && !isOpen}
                    variant='primary'
                    onClick={() => handleClick(course.id)}
                >
                    Enroll
                </Button>
            </Card.Body>
        </Card>
    ));
}

export default CourseList;
