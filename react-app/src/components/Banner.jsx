import { Button, Row, Col } from "react-bootstrap";
import { useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";

function Banner() {
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        if (location.pathname === "/") {
            document.querySelector("#title").textContent = "Zuitt Coding Bootcamp";
            document.querySelector("#body").textContent =
                "Opportunities for everyone, everywhere.";
            document.querySelector("#button").textContent = "Enroll now!";
        } else {
            document.querySelector("#title").textContent = "Page not found 404";
            document.querySelector("#body").textContent = "Go back to the";
            document.querySelector("#button").textContent = "Homepage";
        }
    }, [location]);

    const homepageButton = () => {
        if (location.pathname !== "/") {
            navigate("/");
        } else {
            navigate("/courses");
        }
    };

    return (
        <Row>
            <Col className='p-5'>
                <h1 id='title' />
                <p id='body' style={{ display: "inline-block", marginRight: "10px" }} />
                <Button
                    style={{ display: "inline-block" }}
                    id='button'
                    variant='link'
                    onClick={homepageButton}
                />
            </Col>
        </Row>
    );
}

export default Banner;
