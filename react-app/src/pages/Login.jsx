import { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useNavigate } from "react-router-dom";

function BasicExample() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true);
        }
    });

    function handleSubmit(e) {
        e.preventDefault();

        setEmail("");
        setPassword("");
        navigate("/");
        localStorage.setItem("email", email);

        alert("You are now logged in");
        setIsActive(false);
    }
    return (
        <Form onSubmit={(e) => handleSubmit(e)}>
            <Form.Group className='mb-3' controlId='formBasicEmail'>
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type='email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='Enter email'
                />
                <Form.Text className='text-muted'>
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className='mb-3' controlId='formBasicPassword'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder='Password'
                />
            </Form.Group>
            {isActive ? (
                <Button variant='primary' type='submit'>
                    Submit
                </Button>
            ) : (
                <Button variant='primary' type='submit' disabled>
                    Submit
                </Button>
            )}
        </Form>
    );
}

export default BasicExample;
