import CourseCards from "../components/CourseCards";

export default function Home() {
    return (
        <>
            <CourseCards />
        </>
    );
}
