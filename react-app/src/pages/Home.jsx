import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { useLocation } from "react-router-dom";

export default function Home() {
    const { pathname } = useLocation();
    if (pathname === "/") {
        return (
            <>
                <Banner />
                <Highlights />
            </>
        );
    } else {
        return (
            <>
                <Banner />
            </>
        );
    }
}
