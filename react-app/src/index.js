import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";

// const user = {
//     firstName: "Jane",
//     lastName: "Smith",
// };

// function formatName(user) {
//     return user.firstName + " " + user.lastName;
// }
// const element = <h1>Hello, {formatName(user)}</h1>;

ReactDOM.render(<App />, document.getElementById("root"));
